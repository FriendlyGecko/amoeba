# Amoeba
A simple GUI for quickly changing interfaces on your network.

## Use
In order to properly use it, you will need to compile it yourself and have the JSON files in the directory as the application file.
<br> <br>
First, you have to edit devices.json to list your network devices. Next, you have to edit interface_configs.json to include your intended interface configurations. If you are unfamiliar with JSON you can use the already included JSON objects/arrays as a template for creating your devices and configurations.
<br> <br>
Lastly, you just need to run the program. If you forgot what interfaces you have on the device, you can click "Check Interfaces" and it will display the results of "show ip interface brief" so you can use that to fill in the interface section. You can then use "Check Interface" to see the current running configuration of that interface. The last button will the selected interface configuration to the selected interface on the selected router, but not before defaulting it to a clean slate.
