import tkinter
from tkinter import *
import tkinter.ttk
import netmiko
import json
import serial.tools.list_ports

"""
This application utilizes external JSON files to store device information and interface configurations.
The intent is for the user to be able to query and set interface configurations quickly, cleanly, and without
having to memorize each of the configuration sets they may use. It is able to scaled on the fly in terms of
number of devices and configurations (It may begin to become cumbersome to navigate dropdown menus with large amounts
of either category).
"""


def send_config() -> None:
    """
    This function pushes the configuration if all the fields are filled in.
    It has basic error checking, but would need to be updated if more fields were required and more errors could occur.
    """

    # Check appropriate fields to ensure they are filled out
    if "" in [device_field.get(), user_entry.get(), password_entry.get(), window.int_field.get(),
              int_type_drop.get()]:
        error_window = Tk()
        error_window.title("ERROR")
        message = '''Not all fields are filled out.'''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)
    else:
        interface = window.int_field.get()
        interface_type = int_type_drop.get()
        try:
            net_connect = netmiko.ConnectHandler(username=user_entry.get(), password=password_entry.get(),
                                                 auth_timeout=60,
                                                 **devices[device_field.get()])

        except netmiko.exceptions.NetmikoTimeoutException:
            error_window = Tk()
            error_window.title("ERROR")
            message = '''TCP connection to device failed.

    Common causes of this problem are:
    1. Lack of connectivity to device.
    2. Incorrect hostname or IP address.
    3. Wrong TCP port.
    4. Intermediate firewall blocking access.'''

            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except netmiko.exceptions.NetmikoAuthenticationException:
            error_window = Tk()
            error_window.title("ERROR")
            message = '''Authentication to device failed.

    Common causes of this problem are:
    1. Invalid username and password
    2. Incorrect SSH-key file
    3. Connecting to the wrong device
                '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except ValueError:
            com_ports = list(serial.tools.list_ports.comports())
            ports = []
            for port in com_ports:
                ports.append(port.name)
            error_window = Tk()
            error_window.title("ERROR")
            message = f'''
            Device {str(device_field.get())} not found. 
            Available devices are: {",".join(ports)}
                        '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except serial.serialutil.SerialException:
            error_window = Tk()
            error_window.title("ERROR")
            message = f'''
                        Could not open port {str(device_field.get())}. 
                        'Access is denied.'
                        Potential Causes:
                        1. The port may still be registered as active.
                        2. There is another service utilizing the port.

                    Close other programs that may be using the COM port and reseat the COM port before you try again.
                                    '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        else:
            # This checks to see if the interface even exists
            if "% Invalid input detected at '^' marker." in net_connect.send_command(f"show interface {interface}"):
                error_window = Tk()
                error_window.title("ERROR")
                message = "Interface cannot be found on the selected device. Try checking interfaces first."

                error_label = Label(error_window, text=message)
                ok_btn = Button(error_window, text="OK", command=error_window.destroy)
                error_label.grid(column=0, row=0)
                ok_btn.grid(column=0, row=1)
            else:
                def configure():
                    try:
                        commands = [f"default interface {interface}", f"interface {interface}"]
                        for command in interface_configs[interface_type]:
                            commands.append(command)
                        net_connect.send_config_set(commands)
                    except netmiko.exceptions.ReadTimeout:
                        error = Tk()
                        error.title("ERROR")
                        error_message = """Pattern not detected in output. 
                        It appears that you may have modified an interface connecting you to the device causing you to
                        lose the connection."""

                        label = Label(error, text=error_message)
                        ok = Button(error, text="OK", command=error_window.destroy)
                        label.grid(column=0, row=0)
                        ok.grid(column=0, row=1)
                interface_full = net_connect.send_command(f"show interface {interface}").split(" ", 1)[0]
                self_interface_row = net_connect.send_command("show user").split("\n")
                self_interface = None

                if "com" not in device_field.get().lower():
                    for row in self_interface_row:
                        if "*" in row:
                            self_ip = row.split(" ")[-1]
                            self_mac = net_connect.send_command(f"show arp | include {self_ip}").split(" ")[-6]
                            self_interface_short = net_connect.send_command(
                                f"show mac address-table | include {self_mac}").split(" ")[-1]
                            self_interface = net_connect.send_command(
                                f"show interface {self_interface_short}").split(" ", 1)[0]
                            break
                if self_interface and self_interface == interface_full:
                    error_window = Tk()
                    error_window.title("WARNING")
                    message = '''The interface you are connected to may be the same one you are modifying.
                     Click "Accept Risk" only if you accept the risk, then click close. Just click "Close" to abandon.
                     It is recommended to abort.
                     '''
                    error_label = Label(error_window, text=message)
                    ok_btn = Button(error_window, text="Accept Risk", command=configure)
                    error_label.grid(column=0, row=0)
                    ok_btn.grid(column=0, row=1, sticky=tkinter.NSEW)
                    abort_btn = Button(error_window, text="Close", command=error_window.destroy)
                    error_label.grid(column=0, row=0)
                    abort_btn.grid(column=1, row=1, sticky=NSEW)
                else:
                    configure()
                    if hasattr(window.archive_button, "redefine"):
                        def redefine_button() -> None:
                            window.archive_button.configure(text="Create Archive", command=create_archive,
                                                            background="SystemButtonFace", foreground="Black")

                        window.archive_button.after_cancel(window.archive_button.redefine)
                        window.archive_button.redefine = window.archive_button.after(window.archive_time_ms,
                                                                                     lambda: redefine_button())


def check_interface_config() -> None:
    """
    This just sends "show run interface" to the selected interface and presents it in a new window.
    """

    # Check appropriate fields to ensure they are filled out
    if "" in [device_field.get(), user_entry.get(), password_entry.get(), window.int_field.get()]:
        error_window = Tk()
        error_window.title("ERROR")
        message = ('Not all required fields are filled out. Interface check requires Device Name, Username,'
                   ' and Password.')
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)
    else:
        interface = window.int_field.get()
        try:
            net_connect = netmiko.ConnectHandler(username=user_entry.get(), password=password_entry.get(),
                                                 auth_timeout=60,
                                                 **devices[device_field.get()])

        except netmiko.exceptions.NetmikoTimeoutException:
            error_window = Tk()
            error_window.title("ERROR")
            message = '''TCP connection to device failed.

            Common causes of this problem are:
            1. Lack of connectivity to device.
            2. Incorrect hostname or IP address.
            3. Wrong TCP port.
            4. Intermediate firewall blocking access.'''

            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except netmiko.exceptions.NetmikoAuthenticationException:
            error_window = Tk()
            error_window.title("ERROR")
            message = '''Authentication to device failed.

            Common causes of this problem are:
            1. Invalid username and password
            2. Incorrect SSH-key file
            3. Connecting to the wrong device
                        '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except ValueError:
            com_ports = list(serial.tools.list_ports.comports())
            ports = []
            for port in com_ports:
                ports.append(port.name)
            error_window = Tk()
            error_window.title("ERROR")
            message = f'''
            Device {str(device_field.get())} not found. 
            Available devices are: {",".join(ports)}
                        '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except serial.serialutil.SerialException:
            error_window = Tk()
            error_window.title("ERROR")
            message = f'''
                        Could not open port {str(device_field.get())}. 
                        'Access is denied.'
                        Potential Causes:
                        1. The port may still be registered as active.
                        2. There is another service utilizing the port.
                        
                    Close other programs that may be using the COM port and reseat the COM port before you try again.
                                    '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        else:
            text = net_connect.send_command(f"show run interface {interface}")
            if "% Invalid input detected at '^' marker." in text:
                error_window = Tk()
                error_window.title("ERROR")
                message = "Interface cannot be found on the selected device. Try checking interfaces first."

                error_label = Label(error_window, text=message)
                ok_btn = Button(error_window, text="OK", command=error_window.destroy)
                error_label.grid(column=0, row=0)
                ok_btn.grid(column=0, row=1)
            else:
                information_window = Tk()
                information_window.title(f"{device_field.get()} {interface} Interface")
                information_label = Label(information_window,
                                          text=net_connect.send_command(f"show run interface {interface}"),
                                          justify="left", font=("DejaVu Sans Mono", 10))
                ok_btn = Button(information_window, text="OK", command=information_window.destroy)
                information_label.grid(column=0, row=0)
                ok_btn.grid(column=0, row=1)
                if hasattr(window.archive_button, "redefine"):
                    def redefine_button() -> None:
                        window.archive_button.configure(text="Create Archive", command=create_archive,
                                                        background="SystemButtonFace", foreground="Black")

                    window.archive_button.after_cancel(window.archive_button.redefine)
                    window.archive_button.redefine = window.archive_button.after(window.archive_time_ms,
                                                                                 lambda: redefine_button())


def check_interfaces() -> None:
    """
    This just sends "show ip interface brief" to the selected device and presents it in a new window.
    """

    # Check appropriate fields to ensure they are filled out
    if "" in [device_field.get(), user_entry.get(), password_entry.get()]:
        error_window = Tk()
        error_window.title("ERROR")
        message = ('Not all required fields are filled out. Interface check requires Device Name, Username,'
                   ' and Password.')
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)
    else:
        try:
            net_connect = netmiko.ConnectHandler(username=user_entry.get(), password=password_entry.get(),
                                                 auth_timeout=60, banner_timeout=30,
                                                 **devices[device_field.get()])

        except netmiko.exceptions.NetmikoTimeoutException:
            error_window = Tk()
            error_window.title("ERROR")
            message = '''TCP connection to device failed.

            Common causes of this problem are:
            1. Lack of connectivity to device.
            2. Incorrect hostname or IP address.
            3. Wrong TCP port.
            4. Intermediate firewall blocking access.'''

            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except netmiko.exceptions.NetmikoAuthenticationException:
            error_window = Tk()
            error_window.title("ERROR")
            message = '''Authentication to device failed.

            Common causes of this problem are:
            1. Invalid username and password
            2. Incorrect SSH-key file
            3. Connecting to the wrong device
                        '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except ValueError:
            com_ports = list(serial.tools.list_ports.comports())
            ports = []
            for port in com_ports:
                ports.append(port.name)
            error_window = Tk()
            error_window.title("ERROR")
            message = f'''
            Device {str(device_field.get())} not found. 
            Available devices are: {",".join(ports)}
                        '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        except serial.serialutil.SerialException:
            error_window = Tk()
            error_window.title("ERROR")
            message = f'''
                        Could not open port {str(device_field.get())}. 
                        'Access is denied.'
                        Potential Causes:
                        1. The port may still be registered as active.
                        2. There is another service utilizing the port.

                    Close other programs that may be using the COM port and reseat the COM port before you try again.
                                    '''
            error_label = Label(error_window, text=message)
            ok_btn = Button(error_window, text="OK", command=error_window.destroy)
            error_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)

        else:
            output = net_connect.send_command("show interface description")
            information_window = Tk()
            information_window.title(f"{device_field.get()} Interfaces")
            information_label = Label(information_window, text=output, underline=1,
                                      justify="left", font=("DejaVu Sans Mono", 10))
            ok_btn = Button(information_window, text="OK", command=information_window.destroy)
            information_label.grid(column=0, row=0)
            ok_btn.grid(column=0, row=1)
            rows = output.split("\n")
            interface_list = []
            for row in rows:
                if row.split(" ", 1)[0] != "Interface":
                    interface_list.append(row.split(" ", 1)[0])
            for interface in interface_list:
                if interface == "":
                    interface_list.remove(interface)
            window.int_field.configure(values=interface_list)
            window.int_field.set(interface_list[0])
            if hasattr(window.archive_button, "redefine"):
                def redefine_button() -> None:
                    window.archive_button.configure(text="Create Archive", command=create_archive,
                                                    background="SystemButtonFace", foreground="Black")

                window.archive_button.after_cancel(window.archive_button.redefine)
                window.archive_button.redefine = window.archive_button.after(window.archive_time_ms,
                                                                             lambda: redefine_button())


def write() -> None:
    """
    Performs a write function on the selected network device.
    """

    try:
        net_connect = netmiko.ConnectHandler(username=user_entry.get(), password=password_entry.get(),
                                             auth_timeout=60,
                                             **devices[device_field.get()])

    except netmiko.exceptions.NetmikoTimeoutException:
        error_window = Tk()
        error_window.title("ERROR")
        message = '''TCP connection to device failed.

        Common causes of this problem are:
        1. Lack of connectivity to device.
        2. Incorrect hostname or IP address.
        3. Wrong TCP port.
        4. Intermediate firewall blocking access.'''

        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except netmiko.exceptions.NetmikoAuthenticationException:
        error_window = Tk()
        error_window.title("ERROR")
        message = '''Authentication to device failed.

        Common causes of this problem are:
        1. Invalid username and password
        2. Incorrect SSH-key file
        3. Connecting to the wrong device
                    '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except ValueError:
        com_ports = list(serial.tools.list_ports.comports())
        ports = []
        for port in com_ports:
            ports.append(port.name)
        error_window = Tk()
        error_window.title("ERROR")
        message = f'''
        Device {str(device_field.get())} not found. 
        Available devices are: {",".join(ports)}
                    '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except serial.serialutil.SerialException:
        error_window = Tk()
        error_window.title("ERROR")
        message = f'''
                    Could not open port {str(device_field.get())}. 
                    'Access is denied.'
                    Potential Causes:
                    1. The port may still be registered as active.
                    2. There is another service utilizing the port.

                Close other programs that may be using the COM port and reseat the COM port before you try again.
                                '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    else:
        net_connect.send_command("write memory")
        if hasattr(window.archive_button, "redefine"):
            def redefine_button() -> None:
                window.archive_button.configure(text="Create Archive", command=create_archive,
                                                background="SystemButtonFace", foreground="Black")

            window.archive_button.after_cancel(window.archive_button.redefine)
            window.archive_button.redefine = window.archive_button.after(window.archive_time_ms,
                                                                         lambda: redefine_button())


def create_archive() -> None:
    """
    Creates an archive of the current running-config on the selected network device. The archive waits for five minutes
    of inactivity and then will roll back. It serves as a safeguard for potentially disruptive configuration changes.
    """

    try:
        net_connect = netmiko.ConnectHandler(username=user_entry.get(), password=password_entry.get(),
                                             auth_timeout=60,
                                             **devices[device_field.get()])

    except netmiko.exceptions.NetmikoTimeoutException:
        error_window = Tk()
        error_window.title("ERROR")
        message = '''TCP connection to device failed.

        Common causes of this problem are:
        1. Lack of connectivity to device.
        2. Incorrect hostname or IP address.
        3. Wrong TCP port.
        4. Intermediate firewall blocking access.'''

        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except netmiko.exceptions.NetmikoAuthenticationException:
        error_window = Tk()
        error_window.title("ERROR")
        message = '''Authentication to device failed.

        Common causes of this problem are:
        1. Invalid username and password
        2. Incorrect SSH-key file
        3. Connecting to the wrong device
                    '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except ValueError:
        com_ports = list(serial.tools.list_ports.comports())
        ports = []
        for port in com_ports:
            ports.append(port.name)
        error_window = Tk()
        error_window.title("ERROR")
        message = f'''
        Device {str(device_field.get())} not found. 
        Available devices are: {",".join(ports)}
                    '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except serial.serialutil.SerialException:
        error_window = Tk()
        error_window.title("ERROR")
        message = f'''
                    Could not open port {str(device_field.get())}. 
                    'Access is denied.'
                    Potential Causes:
                    1. The port may still be registered as active.
                    2. There is another service utilizing the port.

                Close other programs that may be using the COM port and reseat the COM port before you try again.
                                '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    else:
        commands = ["archive",
                    "path flash:",
                    "max 1",
                    "end",
                    f"configure terminal revert timer idle {window.archive_time}"
                    ]

        net_connect.send_config_set(commands)

        error_window = Tk()
        error_window.title("Show Archive")
        message = net_connect.send_command("show archive")
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

        window.archive_button.configure(text="Clear Archive", command=clear_archive,
                                        background="Dark Blue", foreground="White")
        window.archive_button.mode = "clear"

        def redefine_button() -> None:
            window.archive_button.configure(text="Create Archive", command=create_archive,
                                            background="SystemButtonFace", foreground="Black")

        window.archive_button.redefine = window.archive_button.after(window.archive_time_ms, lambda: redefine_button())


def clear_archive() -> None:
    """
    Clears out a saved archive and stops the idle timer.
    """

    try:
        net_connect = netmiko.ConnectHandler(username=user_entry.get(), password=password_entry.get(),
                                             auth_timeout=60,
                                             **devices[device_field.get()])

    except netmiko.exceptions.NetmikoTimeoutException:
        error_window = Tk()
        error_window.title("ERROR")
        message = '''TCP connection to device failed.

        Common causes of this problem are:
        1. Lack of connectivity to device.
        2. Incorrect hostname or IP address.
        3. Wrong TCP port.
        4. Intermediate firewall blocking access.'''

        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except netmiko.exceptions.NetmikoAuthenticationException:
        error_window = Tk()
        error_window.title("ERROR")
        message = '''Authentication to device failed.

        Common causes of this problem are:
        1. Invalid username and password
        2. Incorrect SSH-key file
        3. Connecting to the wrong device
                    '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except ValueError:
        com_ports = list(serial.tools.list_ports.comports())
        ports = []
        for port in com_ports:
            ports.append(port.name)
        error_window = Tk()
        error_window.title("ERROR")
        message = f'''
        Device {str(device_field.get())} not found. 
        Available devices are: {",".join(ports)}
                    '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    except serial.serialutil.SerialException:
        error_window = Tk()
        error_window.title("ERROR")
        message = f'''
                    Could not open port {str(device_field.get())}. 
                    'Access is denied.'
                    Potential Causes:
                    1. The port may still be registered as active.
                    2. There is another service utilizing the port.

                Close other programs that may be using the COM port and reseat the COM port before you try again.
                                '''
        error_label = Label(error_window, text=message)
        ok_btn = Button(error_window, text="OK", command=error_window.destroy)
        error_label.grid(column=0, row=0)
        ok_btn.grid(column=0, row=1)

    else:
        net_connect.send_command("configure confirm")
        window.archive_button.configure(text="Create Archive", command=create_archive,
                                        background="SystemButtonFace", foreground="Black")
        if hasattr(window.archive_button, "redefine"):
            window.archive_button.after_cancel(window.archive_button.redefine)


# Window Variables
window = Tk()
window.title("Amoeba")
window.iconbitmap("amoeba.ico")


# Tkinter Widgets
def username_entry_clear() -> None:
    user_entry.delete(0, 999)


def password_entry_clear() -> None:
    password_entry.delete(0, 999)


user_lbl = Button(window, text="Admin", highlightthickness=1, background="Dark Blue", foreground="White",
                  command=username_entry_clear)
password_lbl = Button(window, text="Password", highlightthickness=1, background="Dark Blue", foreground="White",
                      command=password_entry_clear)
user_lbl.config(highlightbackground="Black", highlightcolor="Black")
user_entry = Entry(window, width=35, highlightthickness=1)
user_entry.config(highlightbackground="Black", highlightcolor="Black")
password_lbl.config(highlightbackground="Black", highlightcolor="Black")
password_entry = Entry(window, width=35, show="\u2028", highlightthickness=1)
password_entry.config(highlightbackground="Black", highlightcolor="Black")
device_lbl = Label(window, text="Network Device", highlightthickness=1, background="Dark Blue", foreground="White")
device_lbl.config(highlightbackground="Black", highlightcolor="Black")


# Create the Device List from the json file co-located with main script
with open("devices.json", "r") as device_json:
    try:
        devices = json.load(device_json)
    except json.decoder.JSONDecodeError:
        window.destroy()
        json_error_window = Tk()
        json_error_window.title("ERROR")
        json_message = '''Invalid JSON is preventing devices.json from opening.'''

        json_error_label = Label(json_error_window, text=json_message)
        json_ok_btn = Button(json_error_window, text="OK", command=json_error_window.destroy)
        json_error_label.grid(column=0, row=0)
        json_ok_btn.grid(column=0, row=1)
        json_error_window.mainloop()
    else:
        device_names = []
        for device in devices:
            device_names.append(device)
        device_field = tkinter.ttk.Combobox(window, values=device_names, state="readonly")
        check_button = Button(window, text="Check Interfaces", command=check_interfaces, highlightthickness=1)
        check_button.config(highlightbackground="Black", highlightcolor="Black")
        int_lbl = Label(window, text="Interface", highlightthickness=1)
        int_lbl.config(highlightbackground="Black", highlightcolor="Black", background="Dark Blue", foreground="White")
        window.int_field = tkinter.ttk.Combobox(window)
        int_check_btn = Button(window, text="Check Interface", command=check_interface_config, highlightthickness=1)
        int_check_btn.config(highlightbackground="Black", highlightcolor="Black")
        int_type_lbl = Label(window, text="Interface Type", highlightthickness=1, background="Dark Blue",
                             foreground="White")
        int_type_lbl.config(highlightbackground="Black", highlightcolor="Black")

        # Create interface configuration list
        with open("interface_configs.json", "r") as interface_json:
            try:
                interface_configs = json.load(interface_json)
            except json.decoder.JSONDecodeError:
                window.destroy()
                json_error_window = Tk()
                json_error_window.title("ERROR")
                json_message = '''Invalid JSON is preventing interface_configs.json from opening.'''
                json_error_label = Label(json_error_window, text=json_message)
                json_ok_btn = Button(json_error_window, text="OK", command=json_error_window.destroy)
                json_error_label.grid(column=0, row=0)
                json_ok_btn.grid(column=0, row=1)
                json_error_window.mainloop()
            else:
                interface_types = []
                for config in interface_configs:
                    interface_types.append(config)
                int_type_drop = tkinter.ttk.Combobox(window, values=tuple(interface_types), state="readonly")
                configure_btn = Button(window, text="Send Configuration", command=send_config)
                save_btn = Button(window, text="Write Configuration", command=write)
                window.archive_button = Button(window, text="Create Archive", command=create_archive)
                window.archive_button.mode = "create"
                window.archive_time = 1
                window.archive_time_ms = window.archive_time * 60000

                # Placing the tkinter widgets and setting dropdown menu defaults
                user_lbl.grid(column=0, row=0, sticky=tkinter.NSEW)
                user_entry.grid(column=1, row=0, columnspan=2, sticky=tkinter.NSEW)
                user_entry.focus()
                password_lbl.grid(column=0, row=1, sticky=tkinter.NSEW)
                password_entry.grid(column=1, row=1, columnspan=2, sticky=tkinter.NSEW)
                device_lbl.grid(column=0, row=2, sticky=tkinter.NSEW)
                device_field.grid(column=1, row=2, sticky=tkinter.NSEW)
                device_field.set(device_names[0])
                check_button.grid(column=2, row=2, sticky=tkinter.NSEW)
                int_lbl.grid(column=0, row=3, sticky=tkinter.NSEW)
                window.int_field.grid(column=1, row=3, sticky=tkinter.NSEW)
                int_check_btn.grid(column=2, row=3, sticky=tkinter.NSEW)
                int_type_lbl.grid(column=0, row=4, sticky=tkinter.NSEW)
                int_type_drop.grid(column=1, row=4, sticky=tkinter.NSEW, columnspan=2)
                int_type_drop.set(interface_types[0])
                save_btn.grid(column=0, row=5, sticky=tkinter.NSEW)
                configure_btn.grid(column=1, row=5, sticky=tkinter.NSEW)
                window.archive_button.grid(column=2, row=5, sticky=tkinter.NSEW)

                window.mainloop()